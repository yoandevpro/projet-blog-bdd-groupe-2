const dotenv = require('dotenv').config();

const express = require("express");
const mysql = require("mysql");
const app = express();

const db = mysql.createConnection({

    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_DATABASE
});

db.connect(function(err){
    if(err) throw err;
    console.log("mysql connecté");
});
/*db.end(function(err) {
        if (err) {
          return console.log('error:' + err.message);
        }
        console.log('Close the database connection.');
db.destroy();
*/


/*db.query("CREATE DATABASE blog bdd", function(err, result) {
    if(err) throw err;
    console.log("database created...");    
    });*/


/*app.get('/createposttable', (req, res) => {
    let sql = 'CREATE TABLE administrateurs(id int AUTO_INCREMENT,'
})*/

app.listen(3000, function() {
    console.log('le serveur écoute sur le port 3000');
});