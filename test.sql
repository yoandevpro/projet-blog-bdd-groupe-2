-- MySQL Script generated by MySQL Workbench
-- mar. 10 sept. 2019 13:36:02 CEST
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Administrateur`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Administrateur` (
  `idAdministrateur` INT NOT NULL AUTO_INCREMENT,
  `Pseudo` VARCHAR(45) NOT NULL,
  `Mot de passe` VARCHAR(45) NOT NULL,
  `Email` VARCHAR(90) NOT NULL,
  `Historique` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idAdministrateur`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Posts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Posts` (
  `idPosts` INT NOT NULL AUTO_INCREMENT,
  `Contenu` LONGTEXT NOT NULL,
  `Auteur` VARCHAR(45) NOT NULL,
  `Date` DATE NOT NULL,
  `Image` VARCHAR(45) NOT NULL,
  `Administrateur_idAdministrateur` INT NOT NULL,
  PRIMARY KEY (`idPosts`, `Administrateur_idAdministrateur`),
  INDEX `fk_Posts_Administrateur_idx` (`Administrateur_idAdministrateur` ASC),
  CONSTRAINT `fk_Posts_Administrateur`
    FOREIGN KEY (`Administrateur_idAdministrateur`)
    REFERENCES `mydb`.`Administrateur` (`idAdministrateur`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Commentaires`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Commentaires` (
  `idCommentaires` INT NOT NULL AUTO_INCREMENT,
  `Identifiant` VARCHAR(45) NOT NULL,
  `Auteur` VARCHAR(45) NOT NULL,
  `Contenu` VARCHAR(200) NOT NULL,
  `Date` DATE NOT NULL,
  `Administrateur_idAdministrateur` INT NOT NULL,
  `Posts_idPosts` INT NOT NULL,
  `Posts_Administrateur_idAdministrateur` INT NOT NULL,
  PRIMARY KEY (`idCommentaires`, `Administrateur_idAdministrateur`, `Posts_idPosts`, `Posts_Administrateur_idAdministrateur`),
  INDEX `fk_Commentaires_Administrateur1_idx` (`Administrateur_idAdministrateur` ASC),
  INDEX `fk_Commentaires_Posts1_idx` (`Posts_idPosts` ASC, `Posts_Administrateur_idAdministrateur` ASC),
  CONSTRAINT `fk_Commentaires_Administrateur1`
    FOREIGN KEY (`Administrateur_idAdministrateur`)
    REFERENCES `mydb`.`Administrateur` (`idAdministrateur`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Commentaires_Posts1`
    FOREIGN KEY (`Posts_idPosts` , `Posts_Administrateur_idAdministrateur`)
    REFERENCES `mydb`.`Posts` (`idPosts` , `Administrateur_idAdministrateur`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
